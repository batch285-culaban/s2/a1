package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {

    // Java Collection
    //are a single unit of objects
    //useful for manipulating relevant pieces of data that can be used in different situations, more commonly with loops
    public static void main(String[] args){

        // Array
            // In Java, arrays are container of values of the same type given a predefined amount of values
            // Java Arrays are more rigid, once the size and data type are defined, they can no longer be changed
        // Array Declaration

        //datatype[] identifier = new dataType[numOfElements];
        // "[]" indicates that data type should be able to hold multiple values
        // the "new" keyword is used for non-primitive data types to tell Java to create the said variable
        //

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;

        System.out.println(intArray);
        // This will return the memory address of the array

        //to print the intArray, we need to import the Array Class and use the .toString() Method

        System.out.println(Arrays.toString(intArray));


        // Array declaration with initialization
        // dataType[] identifier = {elementA, elementB, elementC, ...};
        // the compiler automatically specifies the size by counting the number of elements in the array

        String[] names = {"John", "Jane", "Joe"};
//        names[3] = "Joey";
        System.out.println(Arrays.toString(names));

        // Sample Java array Method
        // Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after sort(): " + Arrays.toString(intArray));

        //Multidimensional Array
        //a two-dimensional array, can be described by two lengths nested within each other like a matrix
        // first length is "row", second length is "column"

        String[][] classroom = new String[3][3];

        //First Row
        classroom[0][0] = "Naruto";
        classroom[0][1] = "Sasuke";
        classroom[0][2] = "Sakura";

        //Second Row
        classroom[1][0] = "Linny";
        classroom[1][1] = "Tuck";
        classroom[1][2] = "Ming-Ming";

        //Third Row
        classroom[2][0] = "Harry";
        classroom[2][1] = "Ron";
        classroom[2][2] = "Hermione";

        System.out.println(Arrays.deepToString(classroom));


        // ArrayLists
            //are resizable arrays, wherein elements can be added or removed whenever it is needed
        // Syntax
            //ArrayList<T> identifier = new ArrayList<T>();
            //"<T>" is used to specify that the list can only have one type of objects in a collection
        //ArrayList cannot hold primitive data types, "java wrapper classes" provide a way to use these types of objects
        //java wrapper classes - object version of primitive data types with methods

        // declare an ArrayList
        ArrayList<Integer> numbers = new ArrayList<Integer>();
//        ArrayList<String> students = new ArrayList<String>();

        //add elements
        //arrayListName.add(element)

//        students.add("Cardo");
//        students.add("Luffy");
//        System.out.println(students);

        //Declare an ArrayList with Values
        ArrayList<String> students = new ArrayList<>(Arrays.asList("Jane","Mike"));
        students.add("Cardo");
        students.add("Luffy");
        System.out.println(students);


        //access element
        //arrayListName.get(index);
        System.out.println(students.get(3)); //Luffy

        // add an element on a specific index
        // ArrayListName.add(index, element);
        students.add(0, "Cardi");
        System.out.println(students.get(0));
        System.out.println(students);

        //updating an element
        //arrayListName.set(index,element);
        students.set(1,"Tom");
        System.out.println(students);

        //removing a specific element
        //arrayListName.remove(index);
        students.remove(1);
        System.out.println(students);

        //remove all elemets
        students.clear();
        System.out.println(students);

        //getting the arrayList size
        System.out.println(students.size());

//        students.removeAll(students);
//        System.out.println(students); // same as clear


        // HashMaps
            //most objects in Java are defined and are instantiations of classes that contain a proper set of properties and methods
        //there might be use cases where this is not appropriate, or you may simply want to store a collection of data in key-value pairs
        //in Java "keys" are also referred as "fields"
        //wherein the values are accessed by the fields
        //Syntax
        //HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<>();

        //declare hashmaps
//        HashMap<String,String> jobPosition = new HashMap<String, String>();




        //declare hashmaps with initialization
        HashMap<String,String> jobPosition = new HashMap<>(){

            {
                put("Teacher","Cee");
                put("Web Developer","Peter Parker");
            }
        };

        //add element
        //hashMapName.put(<fieldName>, <value>);

        jobPosition.put("Dreamer", "Morpheus");
        jobPosition.put("Police", "Cardo");
        System.out.println(jobPosition);

        //access element
        //hashMapName.get("fieldName");
        System.out.println(jobPosition.get("Police")); // Key sensitive

        //update the value
        //hashMapName.replace("fieldNameToChange", "newValue");
        jobPosition.replace("Dreamer","Persephone");
        System.out.println(jobPosition);

        //Remove an element
        jobPosition.remove("Dreamer");
        System.out.println(jobPosition);

        //retrieve hashmaps keys in an array
        System.out.println(jobPosition.keySet());
        System.out.println(jobPosition.values());






    }
}