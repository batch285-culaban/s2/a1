package com.zuitt.example;

import java.util.Scanner;

public class WDC043_S2_A1 {
    public static void main(String[] args){

        Scanner theYear = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");
        int inputYear = theYear.nextInt();

        if(inputYear !=0 && inputYear % 4 == 0){
            System.out.println(inputYear + " is a leap year");
            }else{
            System.out.println(inputYear + " is NOT a leap year");
        }
    }
}
